![header image](./images/logo_color_text.svg "Logo with Text")
## About the deserted template
Deserted is a lightweight website template that can be deployed through GitLab Pages.

### Features
- **Lightweight**: deserted features a minimum number of html pages designed for marketing products and services. In addition, while CSS and JavaScript files are included, code is kept to a minimum.

- **Responsive design**: deserted uses CSS Flexbox media queries to create a responsive design.[^1] In other words, the deserted template will optimally adjust content based on the view port sizes of a device.

- **Ease of deployment**: the deserted template can be deployed via GitLab pages without the need for a Static Website Generator, such as Gatsby. The provided `.gitlab-ci.yml` file does all the work by taking the files in taking the files in the root directory, deploying the website artivacts to a "public" path, creating the web pages and then uploading to GitLab Pages for publishing.[^2]

- **Ease of use**: Images have standard names, so changing them only involves adding the standard name to a picture and then pushing to the root directory of your project. Modifying text in the html file is a matter of opening the html file in the text editor of your choice and then finding the correct text to update using the provided comments as a guide.

### View the current deserted template
The deserted project is a solution "under construction." To view how the current deserted template looks published via GitLab Pages, click the link below.

[Click Here to View the deserted Template Published via GitLabs Pages](https://jimvzit.gitlab.io/deserted/ "Published Template")

### References
[^1]: [CSS Flex Responsive](https://www.w3schools.com/css/css3_flexbox_responsive.asp)
[^2]: [Hosting on GitLab.com with GitLab Pages](https://about.gitlab.com/blog/2016/04/07/gitlab-pages-setup/)
